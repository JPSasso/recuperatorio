﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using BLL;
using BE;

namespace Recuperatorio
{
    /*
     Desarrollar en C# y Windows Forms una aplicación en 4 capas que permita:

            X Mostrar los procesos en ejecución
            X Ejecutar aplicaciones con y sin parámetros
            X Guardar el nombre y tiempo de ejecución en una bd 
                de sql server y en un archivo xml
            X Leer y mostrar los procesos almacenados en el archivo xml en la bd.
     */
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ProcessBLL BLL = new ProcessBLL();
        ProcesoP Proceso = new ProcesoP();
        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Enlazar();           
        }
        List<ProcesoP> procesoPs = new List<ProcesoP>();
        private void Enlazar ()
        {
            listBoxProcesos.DataSource = null;
            listBoxProcesos.DataSource = GetProcessesList();
            listBoxProcesos.DisplayMember = "ProcessName";            
        }

        private  void SaveProcesses()
        {
            if (label5.Visible == false)
            {
                List<Process> procesos = Process.GetProcesses().ToList();
                // foreach (Process process in Process.GetProcesses())
                foreach (Process p in procesos)
                {
                    ProcesoP proceso = new ProcesoP()
                    {
                        Proceso = (Process)p,
                        ID = ((Process)p).Id,
                        Name = ((Process)p).ProcessName
                    };
                    try { proceso.Time = ((Process)p).TotalProcessorTime; } catch { }
                    BLL.Alta(proceso);
                }
            }
        }

        private static List<Process> GetProcessesList()
        {
            return Process.GetProcesses().ToList();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DataSet ds = (DataSet)BLL.GetDataSet();
            OFD.InitialDirectory = BLL.GetXMLPath();
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                ds.ReadXml(OFD.FileName);
            }
            dataGridViewxmllog.DataSource = null;
            dataGridViewxmllog.DataSource = ds.Tables[0];
            dataGridViewxmllog.Columns[0].Width = 80;
            dataGridViewxmllog.Columns[1].Width = 230;
            dataGridViewxmllog.Columns[2].Width = 200;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox1.Text))
            {
                StartProcessbutton.Enabled = true;
            }
        }

        private void StartProcessbutton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox2.Text))
            {
                Proceso.StartProcess(textBox1.Text);
                Proceso.Name = Proceso.Proceso.ProcessName;
                Proceso.ID = Proceso.ID;
            }
            else
            {
                Proceso.StartProcess(textBox1.Text, textBox2.Text);
                Proceso.Name = Proceso.Proceso.ProcessName;
                Proceso.ID = Proceso.ID;
            }
            BLL.Alta(Proceso);
            Enlazar();
        }
        
        private void KillProcessbutton_Click(object sender, EventArgs e)
        {
            if (listBoxProcesos.SelectedItem != null)
            {
                ProcesoP Proceso = new ProcesoP();
                Proceso.Proceso = (Process)listBoxProcesos.SelectedItem;
                Proceso.KillandGetExecTime();
                Enlazar();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            label5.Visible = true;
            SaveProcesses(); 
            MessageBox.Show("Alta OK");
            timer1.Start();
            label5.Visible = false;
        }

        private void listBoxProcesos_SelectedIndexChanged(object sender, EventArgs e)
        {
            KillProcessbutton.Enabled = true;
            Process p = (Process)listBoxProcesos.SelectedItem;
            try
            {
                labelname.Text = p.ProcessName;
                labeltitle.Text = p.MainWindowTitle;
            }
            catch (Exception)
            {
                labelname.Text = "Usted no posee permiso para leer esto";
                labeltitle.Text = "Usted no posee permiso para leer esto";
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BLL.MoveXML();
        }

        private void Listbutton_Click(object sender, EventArgs e)
        {
            ListarProcesos();
        }

        private void ListarProcesos()
        {
            dataGridViewDBlog.DataSource = null;
            dataGridViewDBlog.DataSource = BLL.GetProcesos();
            dataGridViewDBlog.Columns[0].Width = 80;
            dataGridViewDBlog.Columns[1].Width = 200;
            dataGridViewDBlog.Columns[2].Width = 0;
            dataGridViewDBlog.Columns[3].Width = 200;
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (!(string.IsNullOrWhiteSpace(textBoxPRocessID.Text)))
            {
                dataGridViewDBlog.DataSource = null;
                dataGridViewDBlog.DataSource = BLL.GetProcesobyID(int.Parse(textBoxPRocessID.Text));
                textBoxPRocessID.Text = string.Empty;
                dataGridViewDBlog.Columns[0].Width = 80;
                dataGridViewDBlog.Columns[1].Width = 200;
                dataGridViewDBlog.Columns[2].Width = 0;
                dataGridViewDBlog.Columns[3].Width = 200;
            }
            if (!(string.IsNullOrWhiteSpace(textBoxProcessname.Text)))
            {
                dataGridViewDBlog.DataSource = null;
                dataGridViewDBlog.DataSource = BLL.GetProcesobyName(textBoxProcessname.Text);
                textBoxProcessname.Text = string.Empty;
                dataGridViewDBlog.Columns[0].Width = 80;
                dataGridViewDBlog.Columns[1].Width = 200;
                dataGridViewDBlog.Columns[2].Width = 0;
                dataGridViewDBlog.Columns[3].Width = 200;
            }
        }

        private void textBoxPRocessID_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace( textBoxPRocessID.Text))
            {
                textBoxProcessname.Enabled = true;
            }
            else
            {
                textBoxProcessname.Enabled = false;
            }
        }

        private void textBoxProcessname_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxProcessname.Text))
            {
                textBoxPRocessID.Enabled = true;
            }
            else
            {
                textBoxPRocessID.Enabled = false;
            }
        }
    }
}
