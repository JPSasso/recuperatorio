﻿namespace Recuperatorio
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBoxProcesos = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageProcesses = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.KillProcessbutton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.StartProcessbutton = new System.Windows.Forms.Button();
            this.tabPageDB = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxProcessname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPRocessID = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.Listbutton = new System.Windows.Forms.Button();
            this.dataGridViewDBlog = new System.Windows.Forms.DataGridView();
            this.tabPageXML = new System.Windows.Forms.TabPage();
            this.OFDbutton = new System.Windows.Forms.Button();
            this.dataGridViewxmllog = new System.Windows.Forms.DataGridView();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.label5 = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.labeltitle = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPageProcesses.SuspendLayout();
            this.tabPageDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDBlog)).BeginInit();
            this.tabPageXML.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewxmllog)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxProcesos
            // 
            this.listBoxProcesos.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxProcesos.FormattingEnabled = true;
            this.listBoxProcesos.ItemHeight = 19;
            this.listBoxProcesos.Location = new System.Drawing.Point(3, 3);
            this.listBoxProcesos.Name = "listBoxProcesos";
            this.listBoxProcesos.Size = new System.Drawing.Size(300, 530);
            this.listBoxProcesos.TabIndex = 0;
            this.listBoxProcesos.SelectedIndexChanged += new System.EventHandler(this.listBoxProcesos_SelectedIndexChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageProcesses);
            this.tabControl1.Controls.Add(this.tabPageDB);
            this.tabControl1.Controls.Add(this.tabPageXML);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(90, 18);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(566, 562);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPageProcesses
            // 
            this.tabPageProcesses.Controls.Add(this.labeltitle);
            this.tabPageProcesses.Controls.Add(this.labelname);
            this.tabPageProcesses.Controls.Add(this.label5);
            this.tabPageProcesses.Controls.Add(this.button1);
            this.tabPageProcesses.Controls.Add(this.KillProcessbutton);
            this.tabPageProcesses.Controls.Add(this.label2);
            this.tabPageProcesses.Controls.Add(this.label1);
            this.tabPageProcesses.Controls.Add(this.textBox2);
            this.tabPageProcesses.Controls.Add(this.textBox1);
            this.tabPageProcesses.Controls.Add(this.StartProcessbutton);
            this.tabPageProcesses.Controls.Add(this.listBoxProcesos);
            this.tabPageProcesses.Location = new System.Drawing.Point(4, 22);
            this.tabPageProcesses.Name = "tabPageProcesses";
            this.tabPageProcesses.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageProcesses.Size = new System.Drawing.Size(558, 536);
            this.tabPageProcesses.TabIndex = 0;
            this.tabPageProcesses.Text = "Procesos";
            this.tabPageProcesses.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(309, 327);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(241, 27);
            this.button1.TabIndex = 4;
            this.button1.Text = "Almacenar Procesos Actuales";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // KillProcessbutton
            // 
            this.KillProcessbutton.Enabled = false;
            this.KillProcessbutton.Location = new System.Drawing.Point(309, 227);
            this.KillProcessbutton.Name = "KillProcessbutton";
            this.KillProcessbutton.Size = new System.Drawing.Size(241, 27);
            this.KillProcessbutton.TabIndex = 3;
            this.KillProcessbutton.Text = "Detener Proceso";
            this.KillProcessbutton.UseVisualStyleBackColor = true;
            this.KillProcessbutton.Click += new System.EventHandler(this.KillProcessbutton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(309, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "Parametros";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(309, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "Proceso";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(309, 93);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(241, 26);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(309, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(241, 26);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // StartProcessbutton
            // 
            this.StartProcessbutton.Enabled = false;
            this.StartProcessbutton.Location = new System.Drawing.Point(309, 125);
            this.StartProcessbutton.Name = "StartProcessbutton";
            this.StartProcessbutton.Size = new System.Drawing.Size(241, 27);
            this.StartProcessbutton.TabIndex = 2;
            this.StartProcessbutton.Text = "Iniciar Proceso";
            this.StartProcessbutton.UseVisualStyleBackColor = true;
            this.StartProcessbutton.Click += new System.EventHandler(this.StartProcessbutton_Click);
            // 
            // tabPageDB
            // 
            this.tabPageDB.Controls.Add(this.label4);
            this.tabPageDB.Controls.Add(this.textBoxProcessname);
            this.tabPageDB.Controls.Add(this.label3);
            this.tabPageDB.Controls.Add(this.textBoxPRocessID);
            this.tabPageDB.Controls.Add(this.buttonSearch);
            this.tabPageDB.Controls.Add(this.Listbutton);
            this.tabPageDB.Controls.Add(this.dataGridViewDBlog);
            this.tabPageDB.Location = new System.Drawing.Point(4, 22);
            this.tabPageDB.Name = "tabPageDB";
            this.tabPageDB.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDB.Size = new System.Drawing.Size(558, 536);
            this.tabPageDB.TabIndex = 1;
            this.tabPageDB.Text = "ConsultasDB";
            this.tabPageDB.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "Process Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxProcessname
            // 
            this.textBoxProcessname.Location = new System.Drawing.Point(288, 87);
            this.textBoxProcessname.Name = "textBoxProcessname";
            this.textBoxProcessname.Size = new System.Drawing.Size(131, 26);
            this.textBoxProcessname.TabIndex = 9;
            this.textBoxProcessname.TextChanged += new System.EventHandler(this.textBoxProcessname_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(142, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Process ID";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxPRocessID
            // 
            this.textBoxPRocessID.Location = new System.Drawing.Point(146, 87);
            this.textBoxPRocessID.Name = "textBoxPRocessID";
            this.textBoxPRocessID.Size = new System.Drawing.Size(131, 26);
            this.textBoxPRocessID.TabIndex = 8;
            this.textBoxPRocessID.TextChanged += new System.EventHandler(this.textBoxPRocessID_TextChanged);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(3, 71);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(137, 42);
            this.buttonSearch.TabIndex = 7;
            this.buttonSearch.Text = "Buscar Proceso\r\n";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // Listbutton
            // 
            this.Listbutton.Location = new System.Drawing.Point(3, 6);
            this.Listbutton.Name = "Listbutton";
            this.Listbutton.Size = new System.Drawing.Size(137, 42);
            this.Listbutton.TabIndex = 6;
            this.Listbutton.Text = "Listar Procesos";
            this.Listbutton.UseVisualStyleBackColor = true;
            this.Listbutton.Click += new System.EventHandler(this.Listbutton_Click);
            // 
            // dataGridViewDBlog
            // 
            this.dataGridViewDBlog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewDBlog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDBlog.Location = new System.Drawing.Point(3, 119);
            this.dataGridViewDBlog.Name = "dataGridViewDBlog";
            this.dataGridViewDBlog.Size = new System.Drawing.Size(552, 411);
            this.dataGridViewDBlog.TabIndex = 0;
            // 
            // tabPageXML
            // 
            this.tabPageXML.Controls.Add(this.OFDbutton);
            this.tabPageXML.Controls.Add(this.dataGridViewxmllog);
            this.tabPageXML.Location = new System.Drawing.Point(4, 22);
            this.tabPageXML.Name = "tabPageXML";
            this.tabPageXML.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageXML.Size = new System.Drawing.Size(558, 536);
            this.tabPageXML.TabIndex = 2;
            this.tabPageXML.Text = "ConsultasXML";
            this.tabPageXML.UseVisualStyleBackColor = true;
            // 
            // OFDbutton
            // 
            this.OFDbutton.Location = new System.Drawing.Point(3, 6);
            this.OFDbutton.Name = "OFDbutton";
            this.OFDbutton.Size = new System.Drawing.Size(129, 42);
            this.OFDbutton.TabIndex = 10;
            this.OFDbutton.Text = "Buscar XML";
            this.OFDbutton.UseVisualStyleBackColor = true;
            this.OFDbutton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dataGridViewxmllog
            // 
            this.dataGridViewxmllog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewxmllog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewxmllog.Location = new System.Drawing.Point(3, 54);
            this.dataGridViewxmllog.Name = "dataGridViewxmllog";
            this.dataGridViewxmllog.Size = new System.Drawing.Size(552, 476);
            this.dataGridViewxmllog.TabIndex = 1;
            // 
            // OFD
            // 
            this.OFD.Filter = "Archivos xml | *.xml";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(309, 357);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 38);
            this.label5.TabIndex = 8;
            this.label5.Text = "Aguarde mientras se \r\nalmacenan los procesos...";
            this.label5.Visible = false;
            // 
            // labelname
            // 
            this.labelname.AutoSize = true;
            this.labelname.Location = new System.Drawing.Point(309, 167);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(111, 19);
            this.labelname.TabIndex = 9;
            this.labelname.Text = "ProcessName";
            // 
            // labeltitle
            // 
            this.labeltitle.AutoSize = true;
            this.labeltitle.Location = new System.Drawing.Point(309, 186);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(39, 19);
            this.labeltitle.TabIndex = 10;
            this.labeltitle.Text = "Title";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(566, 562);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Processes lookout";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageProcesses.ResumeLayout(false);
            this.tabPageProcesses.PerformLayout();
            this.tabPageDB.ResumeLayout(false);
            this.tabPageDB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDBlog)).EndInit();
            this.tabPageXML.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewxmllog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxProcesos;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageProcesses;
        private System.Windows.Forms.TabPage tabPageDB;
        private System.Windows.Forms.TabPage tabPageXML;
        private System.Windows.Forms.DataGridView dataGridViewDBlog;
        private System.Windows.Forms.DataGridView dataGridViewxmllog;
        private System.Windows.Forms.Button OFDbutton;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button StartProcessbutton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button KillProcessbutton;
        private System.Windows.Forms.Button Listbutton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPRocessID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxProcessname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labeltitle;
        private System.Windows.Forms.Label labelname;
    }
}

