﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace BLL
{
    public class ProcessBLL
    {
        ProcessDAL DAL = new ProcessDAL();
        static XMLAssistant XML = new XMLAssistant();


        public string GetXMLPath()
        {
            return XML.path;
        }
        public object GetDataSet()
        {
            return XML.dataset;
        }

        public List<ProcesoP> GetProcesos()
        {
            return DAL.Listar();
        }
        public List<ProcesoP> GetProcesobyID(int id)
        {
            return DAL.GetProcesoByID(id);
        }
        public List<ProcesoP> GetProcesobyName(string name)
        {
            return DAL.GetProcesoByName(name);
        }
        public int Alta(ProcesoP p)
        {
            int rto = DAL.Alta(p);
            if (rto >= 0)
            {
                XML.WriteFile(p);
            }
            return rto;
        }

        public int Modificar(ProcesoP p)
        {
            return DAL.Modificar(p);
        }

        public int Eliminar(ProcesoP p)
        {
            return DAL.Baja(p);
        }
        public List<ProcesoP> GetProcesosDesdeDiagnostics()
        {
            List<ProcesoP> procesos = new List<ProcesoP>();
            foreach (Process P in Process.GetProcesses())
            {
                ProcesoP proceso = new ProcesoP()
                {
                    Proceso = P
                };
                procesos.Add(proceso);
            }
            return procesos;
        }

        public void MoveXML()
        {
            XML.MoveFile();
        }
    }
}
