﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
  
    public class ProcessDAL
    {
       ProcesoP procesoP = new ProcesoP();

        public int Alta(ProcesoP p)
        {
            Acceso acceso = new Acceso();            
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                //@id int, @name varchar(50), @time varchar(50)
                acceso.CrearParametro("@id",p.Proceso.Id),
                acceso.CrearParametro("@name", p.Proceso.ProcessName),
                acceso.CrearParametro("@time", p.Time.ToString())
            };
            return acceso.Escribir("AltaProceso" , parameters, System.Data.CommandType.StoredProcedure);            
        }

        public List<ProcesoP> GetProcesoByName(string name)
        {
            Acceso acceso = new Acceso();
            List<ProcesoP> procesos = new List<ProcesoP>();
            acceso.AbrirConexion();
            SqlDataReader row = acceso.LeerSQLQuery("select * from Proceso where name like '%" + name + "%'");
            while (row.Read())
            {
                ProcesoP proceso = new ProcesoP()
                {
                    ID = int.Parse(row[0].ToString()),
                    Name = row[1].ToString(),
                    Time = TimeSpan.Parse(row[2].ToString())
                };
                procesos.Add(proceso);
            }
            row.Close();
            return procesos;
        }

        public int Baja(ProcesoP p)
        {
            Acceso acceso = new Acceso();
            return acceso.Escribir("BajaProceso", acceso.CrearParametro("@id", p.Proceso.Id), System.Data.CommandType.StoredProcedure);
        }

        public int Modificar(ProcesoP p)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                //@id int, @name varchar(50), @time time(7)
                acceso.CrearParametro("@id",p.Proceso.Id),
                acceso.CrearParametro("@name", p.Proceso.ProcessName),
                acceso.CrearParametro("@time", p.Time.ToString())
            };
            return acceso.Escribir("ModifProceso",parameters,System.Data.CommandType.StoredProcedure);
        }

        public List<ProcesoP> Listar()
        {
            Acceso acceso = new Acceso();
            List<ProcesoP> procesos = new List<ProcesoP>();
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("GetProcesos");
            acceso.CerrarConexion();
            foreach (DataRow r in table.Rows)
            {
                ProcesoP p = new ProcesoP()
                {
                    ID = int.Parse(r[0].ToString()),
                    Name = r[1].ToString(),
                    Time = TimeSpan.Parse(r[2].ToString())
                };
                procesos.Add(p);
            }
            return procesos;
        }

        public List<ProcesoP> GetProcesoByID(int id)
        {
            Acceso acceso = new Acceso();
            List<ProcesoP> procesos = new List<ProcesoP>();
            acceso.AbrirConexion();
            SqlDataReader row = acceso.LeerSQLQuery("select * from Proceso where ID like '"+id.ToString()+"%'");            
            while (row.Read())
            {
                ProcesoP proceso = new ProcesoP()
                {
                    ID = int.Parse(row[0].ToString()),
                    Name = row[1].ToString(),
                    Time = TimeSpan.Parse(row[2].ToString()) 
                };
                procesos.Add(proceso);
            }
            row.Close();
            acceso.CerrarConexion();
            return procesos;
        }
    }
}
