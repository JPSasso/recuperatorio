﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class XMLAssistant
    {
        ProcesoP Proceso = new ProcesoP();
        public DataSet dataset = new DataSet();
        static string date = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString();
        string schema = date +"_‪SchemaProcessLogs.xml";
        string logFile = date +"_ProcessLogs.xml";
        public string path = @"C:\Users\user\Documents\archivos importantes\UAI\2do año\Lenguajes de Ultima generacion\Cursada 2020\Practicas\Recuperatorio\Recuperatorio\Recuperatorio\bin\Debug\XMLLogs\";
        public XMLAssistant()
        {
            dataset.Tables.Clear();
            dataset.Tables.Add(new DataTable("LOG"));
            dataset.Tables["LOG"].Columns.Add(new DataColumn("ID"));
            dataset.Tables["LOG"].Columns.Add(new DataColumn("Name"));
            dataset.Tables["LOG"].Columns.Add(new DataColumn("Time"));
            dataset.WriteXmlSchema(schema);
            dataset.WriteXml(logFile);
        }
        public void WriteFile(ProcesoP p)
        {
            DataRow REG = dataset.Tables[0].NewRow();
            REG[0] = p.ID;
            REG[1] = p.Proceso.ProcessName;            
            REG[2] = p.Time.ToString();
            dataset.Tables[0].Rows.Add(REG);
            dataset.WriteXml(logFile);
        }

        public void MoveFile()
        {
            string xmlFile = logFile;
            string XmlSchema = schema;
            string destinyfile = path + xmlFile;
            try
            {
                File.Move(xmlFile, destinyfile);
            }
            catch (Exception) { }
            File.Delete(XmlSchema);
        }
    }
}
