﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace BE
{
    public class ProcesoP
    {
        private readonly Stopwatch SW;
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Process Proceso { get; set; }
        public TimeSpan Time { get; set; }

        public ProcesoP()
        {
            SW = new Stopwatch();
          //  this.Proceso = new Process();            
        }
        public void StartTime()
            {
            SW.Start();
        }
        public void StartProcess(string name)
        {
            this.Proceso= Process.Start(name);
            SW.Start();
        }
        public void StartProcess(string name, string param)
        {
            this.Proceso = Process.Start(name, param);
            SW.Start();
        }
        public void StopProcess()
        {  
            this.Proceso.Kill();          
            SW.Stop();
            this.Time = SW.Elapsed;
        }
        public TimeSpan GetExecTime()
        {
            this.Time = SW.Elapsed;
            return Time;
        }
        public TimeSpan KillandGetExecTime()
        {
            this.StopProcess();
            return GetExecTime();
        }

        public override string ToString()
        {
            return "Process: " + this.Proceso.Id.ToString() +"_"+ this.Proceso.ProcessName;
        }
    }
}
